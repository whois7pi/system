define(['require','modules/settings_panel','modules/fdn_dialog','dsds_settings'],function(require) {
  

  var SettingsPanel = require('modules/settings_panel');
  var FdnDialog = require('modules/fdn_dialog');
  var DsdsSettings = require('dsds_settings');	//add by chen-liu1@t2mobile.com for bug 2205

  return function ctor_call_fdn_settings_panel() {
    return SettingsPanel({
      onInit: function(panel, options) {
        this._cardIndex = options.cardIndex || DsdsSettings.getIccCardIndexForCallSettings();	//add by chen-liu1@t2mobile.com for bug 2205
        this._conns = window.navigator.mozMobileConnections;
        this._conn = this._conns[this._cardIndex];

        this._elements = {
          panel: panel,
          resetPin2Item: panel.querySelector('#fdn-resetPIN2'),
          simFdnSelect: document.getElementById('fdn-enabled'),
          resetPin2Button: panel.querySelector('#fdn-resetPIN2 button')
        };

        this._elements.simFdnSelect.addEventListener('change',
          this._showToggleFdnDialog.bind(this));
        this._elements.resetPin2Button.onclick =
          this._showChangePin2Dialog.bind(this);

        this.gaiaHeader = document.querySelector('#simpin2-dialog gaia-header');

        this.params = {
          menuClassName: 'menu-button',
          header: { l10nId:'message' },
          items: [{
            name: 'Select',
            l10nId: 'select',
            priority: 2,
            method: function() {}
          }]
        };
      },

      onBeforeShow: function(panel, options) {
        SettingsSoftkey.init(this.params);
        SettingsSoftkey.show();
        options.cardIndex = DsdsSettings.getIccCardIndexForCallSettings();
        if (typeof options.cardIndex !== 'undefined') {
          this._cardIndex = options.cardIndex;
          this._conn = this._conns[this._cardIndex];
        }

        var iccObj = this._getCurrentIccObj();
        if (iccObj) {
          iccObj.oncardstatechange = this._updateFdnStatus.bind(this);
        }

        this._updateFdnStatus();
      },


      _showToggleFdnDialog: function() {
        var action = this._elements.simFdnSelect.value === 'true' ?
          'enable_fdn' : 'disable_fdn';
        var iccObj = this._getCurrentIccObj();
        if (iccObj) {
          if (iccObj.cardState === 'puk2Required') {
            action = 'unlock_puk2';
          }
          this.gaiaHeader.dataset.href = '#call-fdnSettings';
          FdnDialog.show(action, {
            cardIndex: this._cardIndex,	//add by chen-liu1@t2mobile.com for bug 2205
            onsuccess: () => {
              this._updateFdnStatus();
            },
            oncancel: () => {
              this._updateFdnStatus();
            }
          });
        }
      },

      _showChangePin2Dialog: function() {
        this.gaiaHeader.dataset.href = '#call-fdnSettings';
        FdnDialog.show('change_pin2', {
          cardIndex: this._cardIndex,		//add by chen-liu1@t2mobile.com for bug 2205
          onsuccess: () => {
            this._updateFdnStatus();
          },
          oncancel: () => {
            this._updateFdnStatus();
          }
        });
      },

      _updateFdnStatus: function() {
        var iccObj = this._getCurrentIccObj();
        if (iccObj) {
          return iccObj.getCardLock('fdn').then((result) => {
            var enabled = result.enabled;

            this._elements.simFdnSelect.value = enabled;
          });
        }
      },

      _getCurrentIccObj: function() {
        var iccId;
        var iccObj;

        if (this._conn) {
          iccId = this._conn.iccId;
          if (iccId) {
            iccObj = window.navigator.mozIccManager.getIccById(iccId);
          }
        }

        if (!iccObj) {
          console.log('We can\'t find related iccObj in card - ',
            this._cardIndex);
        }

        return iccObj;
      },

      onBeforeHide: function() {
        SettingsSoftkey.hide();
      }
    });
  };
});
