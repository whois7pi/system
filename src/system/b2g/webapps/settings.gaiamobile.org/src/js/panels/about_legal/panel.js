/* global SettingsSoftkey */
/**
 * Used to show about legal panel
 */
define(['require','modules/settings_panel'],function (require) {
    
    var SettingsPanel = require('modules/settings_panel');

    return function ctor_about_legal_panel() {
        function _initSoftKey() {
            var softkeyParams = {
                menuClassName: 'menu-button',
                header: {
                    l10nId: 'message'
                },
                items: [{
                    name: 'Select',
                    l10nId: 'select',
                    priority: 2,
                    method: function () {
                    }
                }]
            };

            SettingsSoftkey.init(softkeyParams);
            SettingsSoftkey.show();
        }

        return SettingsPanel({
            onInit:function(panel){
                navigator.customization.getValue("feature.jrdelabel.sar.on").then((result) => {
                  dump("feature.jrdelabel.sar.on = " + result);
                  var sar_result = (result == undefined) ? false : result;
                  if(sar_result){
                    var healthLegal = panel.querySelector('#_health_Legal');
                    healthLegal.hidden = false;
                  }
                });
            },

            onBeforeShow: function () {
                _initSoftKey();
            },

            onBeforeHide: function () {
                SettingsSoftkey.hide();
            }
        });
    };
});
