/* global PerformanceTestingHelper */
define(['require','modules/dialog_service','modules/wifi_utils','shared/wifi_helper','modules/wifi_context'],function(require) {
  

  var DialogService = require('modules/dialog_service');
  var WifiUtils = require('modules/wifi_utils');
  var WifiHelper = require('shared/wifi_helper');
  var WifiContext = require('modules/wifi_context');
  var wifiManager = WifiHelper.getWifiManager();

  var WifiNetworkList = function(elements,callback) {
    var list = elements.wifiAvailableNetworks;

    var wifiNetworkList = {
      _scanRate: 15000, // 15s after last scan results
      _scanning: false,
      _autoscan: false,
      _panel: null,
      _timerID: null,
      _focusIndex: 0,
      _rescanFlag: false,
      _dialogPanelShow: false,
      _index: {}, // index of all scanned networks
      _networks: {},
      _list: elements.wifiAvailableNetworks,
      _showSearchStatus: function(enabled) {
        list.hidden = enabled;
        elements.infoItem.hidden = !enabled;
      },
      clear: function() {
        // clear the network list
        this._index = {};

        // remove all items except the text expl.
        // and the "search again" button
        var wifiItems = list.querySelectorAll('li');
        var len = wifiItems.length;
        for (var i = len - 1; i >= 0; i--) {
          list.removeChild(wifiItems[i]);
        }
      },

      getFocusIndex: function() {
        this._focusIndex = 0;

        var wifiItems = list.querySelectorAll('li');
        var focus = list.querySelector('li.focus');

        if (!wifiItems || !focus) {
          return;
        }

        var focusNetwork = JSON.parse(focus.dataset.network);
        for (var i = 0; i < wifiItems.length; i++) {
          var network = JSON.parse(wifiItems[i].dataset.network);
          if (focusNetwork.ssid === network.ssid &&
            focusNetwork.security[0] === network.security[0]) {
            this._focusIndex = i;
            return;
          }
        }
      },

      refreshFocus: function() {
        var wifiItems = list.querySelectorAll('li');
        if (wifiItems.length < this._focusIndex + 1) {
          this._focusIndex = wifiItems.length - 1;
        }
        wifiItems[this._focusIndex].classList.add('focus');
        wifiItems[this._focusIndex].scrollIntoView(false);
      },

      newWifiListItem: function(networkKeys, knownNetwork) {
        var network;
        var self = this;

        self.getFocusIndex();
        self.clear();
        // add detected networks
        for (var j = 0; j < networkKeys.length; j++) {
          network = self._networks[networkKeys[j]];

          if (!WifiUtils.wlanEnabled
              && (network.security[0] === 'WAPI-PSK'
                  || network.security[0] === 'WAPI-CERT')) {
            continue;
          }

          var listItem = WifiUtils.newListItem({
            network: network,
            onClick: self._toggleNetwork.bind(self),
            showNotInRange: true,
            knownNetworks: knownNetwork
          });

          // put connected network on top of list
          if (WifiHelper.isConnected(network)) {
            if (list.childNodes.length !== 0) {
              list.insertBefore(listItem, list.childNodes[0]);
            } else {
              list.appendChild(listItem);
            }
          } else {
            list.appendChild(listItem);
          }

          // add composited key to index
          self._index[networkKeys[j]] = listItem;
        }
      },

      scan: function() {
        var self = this;

        // scan wifi networks and display them in the list
        if (this._scanning) {
          return;
        }

        // stop auto-scanning if wifi disabled or the app is hidden
        if (!wifiManager.enabled || document.hidden) {
          this._scanning = false;
          return;
        }

        this._scanning = true;
        var req = WifiHelper.getAvailableAndKnownNetworks();

        req.onsuccess = function onScanSuccess() {
          var allNetworks = req.result;
          var network;

          self._networks = {};
          for (var i = 0; i < allNetworks.length; ++i) {
            network = allNetworks[i];
            var key = WifiUtils.getNetworkKey(network);
            // keep connected network first, or select the highest strength
            if (!self._networks[key] || network.connected) {
              self._networks[key] = network;
            } else {
              if (!self._networks[key].connected &&
                network.relSignalStrength >
                  self._networks[key].relSignalStrength) {
                    self._networks[key] = network;
              }
            }
          }

          var networkKeys = Object.getOwnPropertyNames(self._networks);

          new Promise(function(resolve, reject) {
            // display network list
            if (networkKeys.length) {
              var knownNetwork = [];

              // sort networks by signal strength
              networkKeys.sort(function(a, b) {
                return self._networks[b].relSignalStrength -
                  self._networks[a].relSignalStrength;
              });

              var request = wifiManager.getKnownNetworks();
              request.onsuccess = function() {
                resolve(request.result);
              };

              request.onerror = function(error) {
                reject(error);
              };
            } else {
              // display a "no networks found" message if necessary
              list.appendChild(WifiUtils.newExplanationItem('noNetworksFound'));
            }
          }).then(function (value) {
            self.newWifiListItem(networkKeys, value);
          }, function (error) {
            var knownNet = [];
            self.newWifiListItem(networkKeys, knownNet);
            console.warn('Error : ', error);
            console.warn('scan: could not retrieve any known network.');
          }).then( () => {
            // hide the "Searching" status
            self._showSearchStatus(false);
            if (callback && typeof callback === 'function') {
              callback();
            }
            var liItem = list.querySelector('.focus');
            if (liItem) {
              liItem.classList.remove('focus');
            }

            self.refreshFocus();
            if (self._rescanFlag) {
              self._rescanFlag = false;
              Toaster.showToast({messageL10nId: 'rescanComplete'});
            }

            self._scanning = false;
          });
        };

        req.onerror = function onScanError(error) {
          // always try again.
          self._scanning = false;

          window.setTimeout(self.scan.bind(self), self._scanRate);
        };
      },

      startAutoscanTimer: function() {
        if (this._timerID) {
          window.clearInterval(this._timerID);
        }
        this._timerID =
          window.setInterval(this.scan.bind(this), this._scanRate);
      },

      getWpsAvailableNetworks: function() {
        // get WPS available networks
        var ssids = Object.getOwnPropertyNames(this._networks);
        var wpsAvailableNetworks = [];
        for (var i = 0; i < ssids.length; i++) {
          var network = this._networks[ssids[i]];
          if (WifiHelper.isWpsAvailable(network)) {
            wpsAvailableNetworks.push(network);
          }
        }
        return wpsAvailableNetworks;
      },
      set autoscan(value) {
        this._autoscan = value;
      },
      get autoscan() {
        return this._autoscan;
      },
      get scanning() {
        return this._scanning;
      },
      _toggleNetwork: function(network) {
        var self = this;

        if (WifiContext.currentNetwork &&
          WifiContext.currentNetwork.ssid === network.ssid) {
          network = WifiContext.currentNetwork;
        }

        var keys = WifiHelper.getSecurity(network);
        var security = (keys && keys.length) ? keys.join(', ') : '';
        var sl = Math.min(Math.floor(network.relSignalStrength / 20), 4);

        var req = wifiManager.getKnownNetworks();
        var done = function () {
          var knownNetwork = false;
          var key = WifiUtils.getNetworkKey(network);
          for (let i in req.result) {
            if (WifiUtils.getNetworkKey(req.result[i]) === key) {
               knownNetwork = true;
               break;
            }
          }

          if (WifiHelper.isConnected(network)) {
            // online: show status + offer to disconnect
            DialogService.show('wifi-status', {
              sl: sl,
              network: network,
              security: security,
            }).then(function(result) {
              var type = result.type;
              if (type === 'submit') {
                WifiContext.forgetNetwork(network, function() {
                  Toaster.showToast({messageL10nId: 'networkforget'});
                  self.scan();
                });
              }
            });
          } else if (knownNetwork) {
            // offline, known network: use getKnownNetworks() to filter unknown networks.
            // no further authentication required.
            WifiHelper.setPassword(network);
            WifiContext.associateNetwork(network);
          } else {
            // offline, unknown network: propose to connect
            var key = WifiHelper.getKeyManagement(network);
            switch (key) {
              case 'WEP':
              case 'WPA-PSK':
              case 'WPA-EAP':
              case 'WPA2-PSK':
              case 'WPA/WPA2-PSK':
                DialogService.show('wifi-auth', {
                  sl: sl,
                  security: security,
                  network: network,
                }).then(function(result) {
                  var type = result.type;
                  var authOptions = result.value;
                  if (type === 'submit') {
                    WifiHelper.setPassword(
                      network,
                      authOptions.password,
                      authOptions.identity,
                      authOptions.eap,
                      authOptions.authPhase2,
                      authOptions.certificate,
                      authOptions.keyIndex
                    );

                    network.sim_num = authOptions.simNum;
                    WifiContext.associateNetwork(network);
                  }
                });
                break;
              case 'WAPI-PSK':
              case 'WAPI-CERT':
                break;
              default:
                WifiContext.associateNetwork(network);
                break;
            }
          }
        };

        req.onsuccess = done;
        req.onerror = done;
      }
    };

    var updateItemPosition = function updateConnectingItemPosition(network) {
      var key = WifiUtils.getNetworkKey(network);
      var connectedItem = wifiNetworkList._index[key];
      if (!connectedItem) {
        return;
      }

      if (list.childNodes.length !== 0) {
        list.insertBefore(connectedItem, list.childNodes[0]);
      } else {
        list.appendChild(connectedItem);
      }
      list.childNodes[0].focus();
      var evt = new CustomEvent('refresh');
      window.dispatchEvent(evt);
    };

    var wapiWifiStateChange = function wapiWifiStateChange(event) {
      if (!event.network) {
        return;
      }

      var ssid = event.network.ssid;
      var security = event.network.security[0];
      if (security !== 'WAPI-PSK' && security !== 'WAPI-CERT') {
        return;
      }

      var li = wifiNetworkList._panel.querySelectorAll('li.wifi');
      for (var i = 0; i < li.length; i++) {
        if (ssid === li[i].querySelector('span.ssid').textContent) {
          li[i].querySelector('a').removeAttribute('href');
          li[i].querySelector('a').onclick = e => {
            wifiNetworkList._toggleNetwork(event.network);
            e.stopPropagation();
          };
        }
      }
    };

    // networkStatus has one of the following values:
    // connecting, associated, connected, connectingfailed, disconnected.
    WifiContext.addEventListener('wifiEnabled', event => {
      WifiUtils.updateListItemStatus({
        listItems: wifiNetworkList._index,
        activeItemDOM: list.querySelector('.active'),
        network: event.network,
        networkStatus: event.status
      });
    });

    WifiContext.addEventListener('wifiStatusChange', event => {
      if (event.status === 'connected') {
        updateItemPosition(event.network);
      }

      if (event.status === 'connected' || event.status === 'disconnected') {
        wapiWifiStateChange(event);
        if (list.dataset.ssid === event.network.ssid) {
          list.dataset.ssid = null;
        }
      }

      WifiUtils.updateListItemStatus({
        listItems: wifiNetworkList._index,
        activeItemDOM: list.querySelector('.active'),
        network: event.network,
        networkStatus: event.status
      });
    });

    WifiContext.addEventListener('wifiConnectionInfoUpdate', event => {
      WifiUtils.updateNetworkSignal(event.network, event.relSignalStrength);
    });

    WifiContext.addEventListener('wifiHasInternet', event => {
      WifiUtils.updateHasInternetStatus({
        listItems: wifiNetworkList._index,
        network: event.network,
      });
    });

    return wifiNetworkList;
  };

  return WifiNetworkList;
});
