var supportedKeyCode = [
	'Enter',
	'ArrowUp',
	'ArrowDown',
	'ArrowLeft',
	'ArrowRight',
	'BrowserBack'
	];

function getKeyCode(key) {
  for(var i=0; i<supportedKeyCode.length; i++) {
  	if (supportedKeyCode[i] == key)
  		return i;
  }
  return -1;
}

function onKeyDown(e) {
	switch(getKeyCode(e.key)) {
		case 0://enter
			break;
		case 1://up
			break;
		case 2://down
			break;
		case 3://left
			break;
		case 4://right
			break;
		case 5://BrowserBack
			window.close();
			break;
		default: //key not supported
			break;
	}
}


window.addEventListener("load", function() {
});

window.addEventListener('keydown', onKeyDown);
