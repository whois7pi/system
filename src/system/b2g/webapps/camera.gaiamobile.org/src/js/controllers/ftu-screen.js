define(['require','exports','module','debug','asyncStorage','views/ftu-screen','lib/bind-all'],function(require, exports, module) {


/**
 * Dependencies
 */


var debug = require('debug')('controller:ftu-screen');
var asyncStorage = require('asyncStorage');
var FTUScreenView = require('views/ftu-screen');
var bindAll = require('lib/bind-all');
/**
 * Exports
 */

module.exports = function(app) { return new FTUScreenController(app); };
module.exports.FTUscreenController = FTUScreenController;

function FTUScreenController(app) {
  asyncStorage.getItem('ftu.opened', (value) => {
    if (!value) {
      asyncStorage.setItem('ftu.opened', true);
      this.app = app;
      this.settings = app.settings;
      this.container = app.el;
      bindAll(this);
      this.openFTU();
    }
  });
}

FTUScreenController.prototype.openFTU = function() {
  if (this.view) {
    return;
  }
  this.view = new FTUScreenView();
  this.view.render().appendTo(this.container);
  this.view.on('closeftu', this.closeFTU);
  this.app.emit('ftu:open');
  this.app.on('hidden', this.closeFTU);
};

FTUScreenController.prototype.closeFTU = function() {
  if (this.view) {
    this.view.destroy();
    this.view = null;
  }
  this.app.emit('ftu:close');
};
});
