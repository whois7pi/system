(function(exports) {
  'use strict';

  var NavigationMap = {
    _controls: null,
    _oldFocusNode: null,
    _oldCtrlCnt:0,
    _id: 0, // used to generate nav-data-id
    _focus_index: 0,

    init: function() {
      window.addEventListener(
        "emergency-call-ice-contacts-ready",
        function () {
          NavigationMap.reset();
          NavigationMap.setFocus();
        },
        true);
    },

    reset: function() {
      this._controls = document.querySelectorAll(".li-ice-contact");
      this.reset_controls();
    },

    /*option menu*/
    reset_options: function _reset() {
      NavigationMap._controls =
        document.querySelectorAll('#mainmenu .menu-button.p-pri');
      NavigationMap.reset_controls();
      var focused = document.querySelectorAll('.focus');
      for (var i = 0; i < focused.length; i++) {
        focused[i].classList.remove('focus');
      }
      NavigationMap._controls[0].classList.add('focus');
      NavigationMap._controls[0].focus();

    },

    reset_controls: function() {
      var _controls = this._controls;
      if (_controls.length)
      {
        var i = 0;
        var id = this.generateId();
        var prevId = id;
        var nextId = this.generateId();

        for (i = 0; i < _controls.length; i++) {

          _controls[i].style.setProperty('--nav-left',  -1);
          _controls[i].style.setProperty('--nav-right', -1);
          _controls[i].setAttribute('tabindex', 0);
          _controls[i].setAttribute('data-nav-id', id);

          _controls[i].style.setProperty('--nav-down', nextId);
          _controls[i].style.setProperty('--nav-up',   prevId);

          prevId = id;
          id = nextId;
          nextId = this.generateId();
        }

        var lastCtrl  = _controls[_controls.length - 1];
        var firstCtrl = _controls[0];

        lastCtrl.style.setProperty('--nav-down', firstCtrl.getAttribute('data-nav-id'));
        firstCtrl.style.setProperty('--nav-up',  lastCtrl.getAttribute('data-nav-id'));
      }
    },

    generateId: function(){
      return this._id++;
    },

    setFocus: function(node){
      if (!node)
      {
        node = this._controls;
        if(node.length)
        {
          node = node[0];
        }
        else
        {
          node = null;
        }
      }

      if (node)
      {
        var oldFocusedNode = this.getFocus();

        if (oldFocusedNode) {
          oldFocusedNode.classList.remove('focus');
          node.setAttribute('tabindex', 0);
        }

        node.setAttribute('tabindex', 1);
        node.classList.add('focus');
        node.focus();
        window.focus();
        this.focusedItemChanged(node);
      }
    },

    setFocusedIndex: function(index){
      this._focus_index = index;
    },

    getFocus : function() {
      var focusedNodes = document.querySelectorAll(".focus");
      return focusedNodes.length > 0 ? focusedNodes[0] : null;
    },

    focusedItemChanged: function (node) {
      if (node) {
        var _controls = this._controls;
        for (var i = 0; i < _controls.length; ++i) {
          if (_controls[i] === node) {
            NavigationMap.setFocusedIndex(i);
            break;
          }
        }
      }
      else {
        NavigationMap.setFocusedIndex(0);
      }
    },
  };

  exports.NavigationMap = NavigationMap;
})(this);

