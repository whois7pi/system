(function(exports) {
  'use strict';

  var skCancel = {
    l10nId: 'cancel',
    priority: 1,
    method: function() {
      SearchMessageUI.searchList.innerHTML = '';
      SearchMessageUI.searchInput.value = '';
      Navigation.toPanel('thread-list');
    }
  };

  var skSelect = {
    l10nId: 'select',
    priority: 2
  };

  var SearchMessageUI = {
    init: function () {
      var searchMessage = document.getElementById('searchMessage');
      if (searchMessage.innerHTML.length === 0) {
        var template = Template('searchMessageViewTmpl');
        searchMessage.innerHTML = template.toString();
      }

      this.searchInput = document.getElementById('search-input');
      this.searchBox = document.getElementById('messages-search-form');
      this.searchList = document.getElementById('search-thread-list');
      this.searchInput.addEventListener('focus',
        this.onSearchInputChange.bind(this));
      this.searchInput.addEventListener('blur',
        this.onSearchInputChange.bind(this));

      this.searchInput.addEventListener('input',
        this.onSearchInputContentChange.bind(this));
    },

    searchItem: function(searchInput) {
      if (searchInput.length === 0) {
        return;
      }

      function updateContacts(threadNumber, threadNode) {
        Contacts.findByAddress(threadNumber, function(contacts) {
          if (contacts.length !== 0 && contacts[0].name) {
            threadNode.querySelector('.threadlist-item-title').
              querySelector('bdi').textContent = contacts[0].name;
          }
        });
        SearchMessageUI.searchList.appendChild(threadNode);
      }

      function createNodes(thread) {
        var node = ThreadListUI.createThread(thread);
        var mmsLable = node.querySelector('.mms-icon');
        var smsLabel = node.querySelector('.body-text');
        var checkBox = node.querySelector('.pack-checkbox-large');
        checkBox.classList.add('hide');
        if (thread.lastMessageType === 'mms') {
          smsLabel.classList.add('hide');
        } else {
          mmsLable.classList.add('hide');
        }
        return node;
      }

      function searchContacts(threadNumber, thread) {
        Contacts.findByAddress(threadNumber, function(contacts) {
          if (contacts.length !== 0) {
           if (contacts[0].name[0].indexOf(searchInput) >= 0) {
              var node = createNodes(thread);
              node.querySelector('.threadlist-item-title').
                querySelector('bdi').textContent = contacts[0].name;
              SearchMessageUI.searchList.appendChild(node);
            }
          }
        });
      }

      function onRenderThread(thread) {
        for (var i = 0; i < thread.participants.length; i++) {
          if (thread.participants[i].indexOf(searchInput) >= 0) {
            var node = createNodes(thread);
            updateContacts(thread.participants[i], node);
          } else {
            searchContacts(thread.participants[i], thread);
          }
        }
      }

      MessageManager.getThreads({
        each: onRenderThread.bind(this)
      });
    },

    onSearchInputChange: function(e) {
      var searchFocus = this.searchBox.classList.contains('search-focus');
      this.searchBox.classList.toggle('search-focus', e.type === 'focus');
      this.updateSKs(searchFocus);
    },

    onSearchInputContentChange: function(event) {
      if (!event.isComposing) {
        this.searchList.innerHTML = '';
        this.searchItem(this.searchInput.value);
      }
    },

    beforeLeave: function() {
      window.removeEventListener('keydown', SearchMessageUI.handleKeyEvent);
    },

    afterEnter: function() {
      this.updateSKs(false);
      window.addEventListener('keydown', SearchMessageUI.handleKeyEvent);
    },

    handleKeyEvent: function(e) {
      switch(e.key) {
        case 'Backspace':
          e.preventDefault();
          SearchMessageUI.searchList.innerHTML = '';
          SearchMessageUI.searchInput.value = '';
          Navigation.toPanel('thread-list');
          break;
        case 'Accept':
        case 'Enter':
          e.preventDefault();
          if (document.activeElement.classList.contains('threadlist-item')) {
            var threadId = document.activeElement.id;
            // Change id type from string to number.
            var id = +threadId.replace('thread-', '');
            SearchMessageUI.searchList.innerHTML = '';
            SearchMessageUI.searchInput.value = '';
            Navigation.toPanel('thread', { id: id });
          }
          break;
      }
    },

    updateSKs: function(isFocus) {
      var params = {
        header: {l10nId: 'options'},
        items: [skCancel]
      };

      if (isFocus) {
        params.items.push(skSelect);
      }

      if (exports.option) {
        exports.option.initSoftKeyPanel(params);
      } else {
        exports.option = new SoftkeyPanel(params);
      }
      exports.option.show();
    }
  };

  exports.SearchMessageUI = SearchMessageUI;
}(this));
