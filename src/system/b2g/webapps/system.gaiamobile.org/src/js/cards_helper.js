'use strict';(function(exports){var HVGA;function isHVGA(){if(undefined===HVGA){HVGA=document.documentElement.clientWidth<480;}
return HVGA;}
function getIconURIForApp(app){if(!app){return null;}
var icons=app.manifest&&app.manifest.icons;var iconPath;if(icons){var sizes=Object.keys(icons).map(function parse(str){return parseInt(str,10);});sizes.sort(function(x,y){return y-x;});var index=sizes[(HVGA)?sizes.length-1:0];iconPath=icons[index];}else{iconPath=app.icon;}
if(!iconPath){return null;}
if(iconPath.charAt(0)==='/'){var base=new URL(app.origin);var port=base.port?(':'+base.port):'';iconPath=base.protocol+'//'+base.hostname+port+iconPath;}
return iconPath;}
function getOffOrigin(src,origin){src=src||origin;var cacheKey=JSON.stringify(Array.prototype.slice.call(arguments));if(!getOffOrigin.cache[cacheKey]){var native=new URL(origin);var current=new URL(src);if(current.protocol=='http:'){getOffOrigin.cache[cacheKey]=current.protocol+'//'+
current.hostname;}else if(native.protocol==current.protocol&&native.hostname==current.hostname&&native.port==current.port){getOffOrigin.cache[cacheKey]='';}else if(current.protocol=='app:'){getOffOrigin.cache[cacheKey]='';}else{getOffOrigin.cache[cacheKey]=current.protocol+'//'+
current.hostname;}}
return getOffOrigin.cache[cacheKey];}
getOffOrigin.cache={};function escapeHTML(str,escapeQuotes){var stringHTML=str;stringHTML=stringHTML.replace(/</g,'&#60;');stringHTML=stringHTML.replace(/(\r\n|\n|\r)/gm,'<br/>');stringHTML=stringHTML.replace(/\s\s/g,' &nbsp;');if(escapeQuotes){return stringHTML.replace(/"/g,'&quot;').replace(/'/g,'&#x27;');}
return stringHTML;}
exports.CardsHelper={getIconURIForApp:getIconURIForApp,getOffOrigin:getOffOrigin,escapeHTML:escapeHTML,isHVGA:isHVGA};})(window);