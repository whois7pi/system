define([ "require", "./oauth2/fetch", "cards", "./base", "template!./setup_fix_oauth2.html" ], function(e) {
    var t, n = e("./oauth2/fetch"), o = e("cards");
    return [ e("./base")(e("template!./setup_fix_oauth2.html")), {
        extraClasses: [ "anim-fade", "anim-overlay" ],
        onArgs: function(e) {
            this.account = e.account, this.restoreCard = e.restoreCard, this.oauth2Name.textContent = this.account.name, 
            t = this, this.setSoftkey();
        },
        setSoftkey: function() {
            var e = [];
            e.push({
                name: "",
                l10nId: "setup-fix-oauth2-reauthorize",
                priority: 2,
                method: function() {
                    t.onReauth();
                }
            }), NavigationMap.setSoftKeyBar(e);
        },
        die: function() {},
        onReauth: function() {
            var e = this.account._wireRep.credentials.oauth2;
            n(e, {
                login_hint: this.account.username
            }).then(function(e) {
                "cancel" === e.status ? this.delayedClose() : "success" === e.status ? (this.account.modifyAccount({
                    oauthTokens: e.tokens
                }), this.account.clearProblems(), this.delayedClose()) : (console.error("Unknown oauthFetch status: " + e.status), 
                this.delayedClose());
            }.bind(this));
        },
        delayedClose: function() {
            setTimeout(this.close.bind(this), 100);
        },
        close: function(e) {
            e && (e.stopPropagation(), e.preventDefault()), o.removeCardAndSuccessors(this, "animate", 1, this.restoreCard);
        }
    } ];
});