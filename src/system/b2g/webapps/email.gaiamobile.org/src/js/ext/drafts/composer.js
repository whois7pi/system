define([ "mailbuild", "../mailchew", "../util" ], function(e, n, t) {
    function o(e) {
        return e.replace(/\r?\n|\r/g, "\r\n");
    }
    function r(t, r, a) {
        var i = this.header = t.header, c = this.body = t.body;
        this.account = r, this.identity = a, this.sentDate = new Date(this.header.date), 
        this._smartWakeLock = null, this.messageId = "<" + Date.now() + Math.random().toString(16).substr(1) + "@mozgaia>";
        var l, d = c.bodyReps[0].content[1];
        if (2 === c.bodyReps.length) {
            var u = c.bodyReps[1].content;
            l = new e("text/html"), l.setContent(o(n.mergeUserTextWithHTML(d, u)));
        } else l = new e("text/plain"), l.setContent(o(d));
        var f;
        c.attachments.length ? (f = this._rootNode = new e("multipart/mixed"), f.appendChild(l)) : f = this._rootNode = l, 
        f.setHeader("From", s([ this.identity ])), f.setHeader("Subject", i.subject), this.identity.replyTo && f.setHeader("Reply-To", this.identity.replyTo), 
        i.to && i.to.length && f.setHeader("To", s(i.to)), i.cc && i.cc.length && f.setHeader("Cc", s(i.cc)), 
        i.bcc && i.bcc.length && f.setHeader("Bcc", s(i.bcc)), f.setHeader("User-Agent", "GaiaMail/0.2"), 
        f.setHeader("Date", this.sentDate.toUTCString()), f.setHeader("Message-Id", this.messageId), 
        c.references && f.setHeader("References", c.references), f.setHeader("Content-Transfer-Encoding", "quoted-printable"), 
        this._blobReplacements = [], this._uniqueBlobBoundary = "{{blob!" + Math.random() + Date.now() + "}}", 
        c.attachments.forEach(function(n) {
            try {
                var t = new e(n.type, {
                    filename: n.name
                });
                t.setHeader("Content-Transfer-Encoding", "base64"), t.setContent(this._uniqueBlobBoundary), 
                f.appendChild(t), this._blobReplacements.push(new Blob(n.file));
            } catch (o) {
                console.error("Problem attaching attachment:", o, "\n", o.stack);
            }
        }.bind(this));
    }
    var s = t.formatAddresses;
    return e.prototype.removeHeader = function(e) {
        for (var n = 0, t = this._headers.length; t > n; n++) if (this._headers[n].key === e) {
            this._headers.splice(n, 1);
            break;
        }
    }, r.prototype = {
        getEnvelope: function() {
            return this._rootNode.getEnvelope();
        },
        withMessageBlob: function(e, n) {
            var t = "Bcc-Temp", o = /^Bcc-Temp: /m, r = e.includeBcc && this.header.bcc && this.header.bcc.length;
            r ? this._rootNode.setHeader(t, s(this.header.bcc)) : this._rootNode.removeHeader(t);
            var a = this._rootNode.build();
            e.smtp && (a = a.replace(/\n\./g, "\n..")), r && (a = a.replace(o, "Bcc: ")), "\r\n" !== a.slice(-2) && (a += "\r\n");
            var i = a.split(btoa(this._uniqueBlobBoundary) + "\r\n");
            this._blobReplacements.forEach(function(e, n) {
                i.splice(2 * n + 1, 0, e);
            }), n(new Blob(i, {
                type: this._rootNode.getHeader("content-type")
            }));
        },
        setSmartWakeLock: function(e) {
            this._smartWakeLock = e;
        },
        renewSmartWakeLock: function(e) {
            this._smartWakeLock && this._smartWakeLock.renew(e);
        }
    }, {
        Composer: r
    };
});