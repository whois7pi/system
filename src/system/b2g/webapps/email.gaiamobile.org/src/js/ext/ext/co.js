define([ "require" ], function() {
    function e(e) {
        var n = this;
        return "function" == typeof e && (e = e.call(this)), new Promise(function(o, r) {
            function s(t) {
                var n;
                try {
                    n = e.next(t);
                } catch (o) {
                    return r(o);
                }
                c(n);
            }
            function a(t) {
                var n;
                try {
                    n = e.throw(t);
                } catch (o) {
                    return r(o);
                }
                c(n);
            }
            function c(e) {
                if (e.done) return o(e.value);
                var r = t.call(n, e.value);
                return r && i(r) ? r.then(s, a) : a(new TypeError('You may only yield a function, promise, generator, array, or object, but the following object was passed: "' + String(e.value) + '"'));
            }
            s();
        });
    }
    function t(t) {
        return t ? i(t) ? t : a(t) || s(t) ? e.call(this, t) : "function" == typeof t ? n.call(this, t) : Array.isArray(t) ? o.call(this, t) : c(t) ? r.call(this, t) : t : t;
    }
    function n(e) {
        var t = this;
        return new Promise(function(n, o) {
            e.call(t, function(e, t) {
                return e ? o(e) : (arguments.length > 2 && (t = d.call(arguments, 1)), n(t), void 0);
            });
        });
    }
    function o(e) {
        return Promise.all(e.map(t, this));
    }
    function r(e) {
        function n(e, t) {
            o[t] = void 0, s.push(e.then(function(e) {
                o[t] = e;
            }));
        }
        for (var o = new e.constructor(), r = Object.keys(e), s = [], a = 0; a < r.length; a++) {
            var c = r[a], d = t.call(this, e[c]);
            d && i(d) ? n(d, c) : o[c] = e[c];
        }
        return Promise.all(s).then(function() {
            return o;
        });
    }
    function i(e) {
        return "function" == typeof e.then;
    }
    function s(e) {
        return "function" == typeof e.next && "function" == typeof e.throw;
    }
    function a(e) {
        var t = e.constructor;
        return t ? "GeneratorFunction" === t.name || "GeneratorFunction" === t.displayName ? !0 : s(t.prototype) : !1;
    }
    function c(e) {
        return Object == e.constructor;
    }
    var d = Array.prototype.slice;
    return e["default"] = e.co = e, e.wrap = function(t) {
        function n() {
            return e.call(this, t.apply(this, arguments));
        }
        return n.__generatorFunction__ = t, n;
    }, e;
});