"use strict";const EXPORTED_SYMBOLS=["WebNavigationFrames"];var Ci=Components.interfaces;function getWindowId(window){return window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindowUtils).outerWindowID;}
function getParentWindowId(window){return getWindowId(window.parent);}
function docShellToWindow(docShell){return docShell.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);}
function convertDocShellToFrameDetail(docShell){let window=docShellToWindow(docShell);return{windowId:getWindowId(window),parentWindowId:getParentWindowId(window),url:window.location.href,};}
function*iterateDocShellTree(docShell){let docShellsEnum=docShell.getDocShellEnumerator(Ci.nsIDocShellTreeItem.typeContent,Ci.nsIDocShell.ENUMERATE_FORWARDS);while(docShellsEnum.hasMoreElements()){yield docShellsEnum.getNext();}
return null;}
function getFrameId(window){let docShell=window.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDocShell);if(!docShell.sameTypeParent){return 0;}
let utils=window.getInterface(Ci.nsIDOMWindowUtils);return utils.outerWindowID;}
function findDocShell(frameId,rootDocShell){for(let docShell of iterateDocShellTree(rootDocShell)){if(frameId==getFrameId(docShellToWindow(docShell))){return docShell;}}
return null;}
var WebNavigationFrames={iterateDocShellTree,findDocShell,getFrame(docShell,frameId){let result=findDocShell(frameId,docShell);if(result){return convertDocShellToFrameDetail(result);}
return null;},getFrameId,getAllFrames(docShell){return Array.from(iterateDocShellTree(docShell),convertDocShellToFrameDetail);},getWindowId,getParentWindowId,};