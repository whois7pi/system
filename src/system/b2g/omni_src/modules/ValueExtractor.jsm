'use strict';const{classes:Cc,interfaces:Ci}=Components;function ValueExtractor(aConsole,aBundle){this.console=aConsole;this.domBundle=aBundle;}
ValueExtractor.prototype={


extractValue({expectedType,object,objectName,property,trim}){const value=object[property];const isArray=Array.isArray(value);const type=(isArray)?'array':typeof value;if(type!==expectedType){if(type!=='undefined'){this.console.warn(this.domBundle.formatStringFromName("ManifestInvalidType",[objectName,property,expectedType],3));}
return undefined;}
const shouldTrim=expectedType==='string'&&value&&trim;if(shouldTrim){return value.trim()||undefined;}
return value;},extractColorValue(spec){const value=this.extractValue(spec);const DOMUtils=Cc['@mozilla.org/inspector/dom-utils;1'].getService(Ci.inIDOMUtils);let color;if(DOMUtils.isValidCSSColor(value)){color=value;}else if(value){this.console.warn(this.domBundle.formatStringFromName("ManifestInvalidCSSColor",[spec.property,value],2));}
return color;}};this.ValueExtractor=ValueExtractor;this.EXPORTED_SYMBOLS=['ValueExtractor'];